print("Input Number of words : " ,end=" " )
no_of_words = int(input())
dict_word = {}
list_of_words = []

for i in range(no_of_words):
  print(f"Enter Word {i+1} : ", end=" ")
  word = input()
  list_of_words.append(word)
  if word in dict_word:
    dict_word[word] += 1
  else:
    dict_word[word] = 1
    
print(f"Total Number of distinct Words : {len(dict_word)}")
print("Order of Word repetition")
for word in dict_word:
  num=dict_word[word]
  print(num, end =" ")
print("\n")
