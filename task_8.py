import statistics
import numpy as np
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--location', type = str, help='Path of msg_latencny.txt File')


args = parser.parse_args()
list= []
sum=0
with open(args.location) as fh:
	for n in fh:
		w=int(n)/1000
		list.append(int(w))

print(f"Mean   : {np.mean(list)}us")
print(f"Median : {np.median(list)}us")
print(f"Max    : {max(list)}us")
print(f"Min    : {min(list)}us")
print(f"Standard Deviation   {np.std(list)}us")
print(f"90th percentile of list    : {np.percentile(list, 90)}us")
print(f"99th percentile of list    : {np.percentile(list, 99)}us")
print(f"99.9th percentile of list  : {np.percentile(list, 99.9)}us")
print(f"99.99th percentile of list : {np.percentile(list, 99.99)}us")
print(f"99.999th percentile of list : {np.percentile(list, 99.999)}us")
