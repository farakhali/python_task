import  matplotlib.pyplot as plt

x_value = [1,2,3,4,5,6,7,8,9,10]

def square(x_value):
        squared_list =  [number ** 2 for number in x_value]
        return squared_list

def cube(x_value):
        cubed_list = [number ** 3 for number in x_value]
        return cubed_list

square_number = square(x_value)
cube_number = cube(x_value)

print("input Number: ", x_value)
print("Square of input number: ",square_number)
print("Cube of input numbers: ",cube_number)

plt.plot(x_value, square_number)
plt.plot(x_value, cube_number)

plt.show()


