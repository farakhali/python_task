import time
import json
import asyncio
import websockets
import argparse
import logging


async def client(arguments):
    # Open JSON file and read data
    logger.info("opening client file")
    read_file = open(arguments.location, "r")
    logger.info("Reading message from file")
    message = json.loads(read_file.read())
    logger.info("Client messages file closed")
    read_file.close()
    async with websockets.connect("ws://localhost:9000") as websocket:
        logger.info("Connected with server")
        for d in message:
            d['tx_time'] = time.time_ns()
            logger.debug(f"sending : {d}")
            await websocket.send(json.dumps(d))
            message = json.loads(await websocket.recv())
            logger.debug(f"recevied message {message}")
    logger.info("Closing connection")


parser = argparse.ArgumentParser(description="websocket client side script")

parser.add_argument('--location', action="store", type = str, help='Path of client_message.json File')
parser.add_argument("-c", "--console_log", action="store_false", default=True, help="to stop log give this flag")
parser.add_argument("-f", "--file_log", action="store_true", default=False, help="to save log give this flag")

args = parser.parse_args()

logger = logging.getLogger("client_log")
logger.setLevel(level=logging.DEBUG)

if args.file_log  == True:
    file_handler = logging.FileHandler(filename='client_log.log')
    file_handler.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
elif args.console_log:
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(stream_handler)


asyncio.run(client(args))


#python3 client_copy.py --location ./client_messages.json -f -c
