import asyncio
import os
import time
import pprint
import logging


logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(message)s')

async def async_find_divisibles(in_range, divisor):
    logging.info(f"find_divisibles called with range {in_range} and divisor {divisor}" )
    t_s=time.time()
    mylist = []
    for i in range(1, in_range):
        if i % divisor == 0:
            await asyncio.sleep(0)
            mylist.append(i)
    #print(mylist, '\n')
    t_f=time.time()
    currentTime = (t_f) - (t_s)
    #print(currentTime)
    logging.info(f"async_find_divisibles ended with range {in_range} and divisor {divisor}. It took {currentTime} seconds" )
    return mylist
""""
async def main():
    lista,listb,listc=await asyncio.gather(
    async_find_divisibles(50800000, 34113),
    async_find_divisibles(100052, 3210),
    async_find_divisibles(500, 3)
    )
    pprint.pprint("Find Divisible (100052,3210) function list")
    pprint.pprint(listb)
    pprint.pprint("Find Divisible (500,3) function list")
    pprint.pprint(listc)
"""

async def main():
    loop = asyncio.get_event_loop()
    t1=loop.create_task(async_find_divisibles(50800000,34113))
    t2=loop.create_task(async_find_divisibles(100052,3210))
    t3=loop.create_task(async_find_divisibles(500,3))
    lista = await t1
    listb = await t2
    listc = await t3
    #pprint.pprint("Find Divisible (100052,3210) function list")
    #pprint.pprint(listb)
    #pprint.pprint("Find Divisible (500,3) function list")
    #pprint.pprint(listc)

if __name__ == "__main__":
    t1= time.perf_counter()
    asyncio.run(main())
    t2=time.perf_counter()
    logging.info(f"total time to execute main {t2-t1}")
    
