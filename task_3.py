import os
import psutil

dir = ("/home/emumba/Details")
CHECK_FOLDER = os.path.isdir(dir)

# If folder doesn't exist, then create it.
if not CHECK_FOLDER:
    os.makedirs(dir)
    print("created folder : ", dir)    
else:
    print(dir, "folder already exists.")
    os.chdir=dir


f = open("/home/emumba/Details/summary.txt", "w")

f.write(os.popen('lscpu | grep -i  byte\ order').read())
f.write(os.popen('lscpu | grep -i  Core*\ per\ Socket').read())
f.write(os.popen('lscpu | grep -i  Socket').read())
f.write(os.popen('lscpu | grep -i  Model\ name').read())
f.write(os.popen('lscpu | grep -i  CPU\ Mhz').read())
f.write(os.popen('lscpu | grep -i  CPU\ max').read())
f.write(os.popen('lscpu | grep -i  CPU\ min').read())
f.write(os.popen('lscpu | grep -i  Virtualization').read())
f.write(os.popen('lscpu | grep -i  L1i\ cache').read())
f.write(os.popen('lscpu | grep -i  L1d\ cache').read())
f.write(os.popen('lscpu | grep -i  L2\ cache').read())
f.write(os.popen('lscpu | grep -i  L3\ cache').read())

print("System Information Updated")
f.close()
