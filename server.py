import json
import asyncio
import logging
import websockets
import time
import argparse

async def server_func(websocket, path):
    logger.info("Connecting......")
    async for message in websocket:
        logger.debug(f"Recived message: {message}")
        ts = time.time_ns()
        message = json.loads(message)
        message['rx_time'] = ts
        msg_latency = (ts - message["tx_time"])
        message['msg_latency'] = msg_latency
        logger.debug(f"Response sen: {message}")
        await websocket.send(json.dumps(message))
        with open("msg_latency.txt", "a") as file:
            file.write(str(msg_latency) + "\n")
    logger.info("Connection closed")


async def main():
    async with websockets.serve(server_func, "localhost", 9000):
        await asyncio.Future()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--console_log", action="store_false", default=True, help="to stop log give this flag")
    parser.add_argument("-f", "--file_log", action="store_true", default=False, help="to save log give this flag")
    args = parser.parse_args()

    logger = logging.getLogger("server_log")
    logger.setLevel(level=logging.DEBUG)

    if args.file_log== True:
        fileh_handler = logging.FileHandler(filename='server_log.log')
        fileh_handler.setLevel(logging.DEBUG)
        logger.addHandler(fileh_handler)
    elif args.console_log:
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.INFO)
        logger.addHandler(stream_handler)

    asyncio.run(main())



#python3 server_copy.py -f
