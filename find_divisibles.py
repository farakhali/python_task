#import math
import asyncio
import os
import time
import pprint
import logging

logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(message)s')
myList = []

def find_divisibles(in_range, divisor):
    
    logging.info(f"find_divisibles called with range {in_range} and divisor {divisor}" )
    t_s=time.time()
    mylist = []
    for i in range(1, in_range):
        if i % divisor == 0:
            mylist.append(i)
    #print(mylist, '\n')
    t_f=time.time()
    currentTime = (t_f) - (t_s)
    #print(currentTime)
    logging.info(f"find_divisibles ended with range {in_range} and divisor {divisor}. It took {currentTime} seconds")

    return mylist


def main():
    t1=time.perf_counter()
    myList_a = find_divisibles(50800000, 34113)
    myList_b = find_divisibles(100052, 3210)
    myList_c = find_divisibles(500, 3)
    pprint.pprint("Find Divisible (100052,3210) function list")
    #pprint.pprint(myList_b)
    pprint.pprint("Find Divisible (500,3) function list")
    #pprint.pprint(myList_c)
    t2=time.perf_counter()
    logging.info(f"total time to execute main {t2-t1}")
main()    
