
import argparse
import sys
import os
import json
import numpy as np


import math
import random

PI = 3.1415926
e = 2.71828
iterator = 0

def run_experiment(n):
    points = [(random.uniform(-1,1), random.uniform(-1,1)) for _ in range(n)]
    points_inside_circle = [(x,y)  for (x,y) in points if x**2 + y**2 <= 1]
    return 4 * len(points_inside_circle) / len(points)

def monte_carlo(N):
    results = []
    avg_pi_values = []
    for _ in range(N):
        results.append(run_experiment(N))
        avg_pi = sum(results) / len(results)
        avg_pi_values.append(avg_pi)
        return avg_pi_values

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--iterations', type = int, required = False)
parser.add_argument('-j', action = 'store_const', const = None, required = False, help = 'Command to extract the number of iterations to run from the adjacent JSON file')

#print(sys.argv, '\n')
args = parser.parse_args()

if args.iterations != None:
    iterator = args.iterations

if sys.argv[1] == '-j':
    myfile=open('task_4.json','r')
    jsondata=myfile.read()    
    obj=json.loads(jsondata)
    iterator =int(obj['num'])


 #   with open("temp.json", "r") as read_file:
  #      data = json.load(read_file)
   #     print(data[1])
        

print(f"Value of Iterator : {iterator}")
pi = monte_carlo(int(iterator))
print(f"Value of PI from Monte Carlo Method : {pi}")
