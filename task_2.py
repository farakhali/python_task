import argparse
import math

x,y= 0,0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--up", help="Up number")
    parser.add_argument("--down", help="Down number")
    parser.add_argument("--left", help="Left number")
    parser.add_argument("--right", help="Right number")

    args = parser.parse_args()

    print(args.up)
    print(args.down)
    print(args.left)
    print(args.right)

    y += int(args.up)
    y -= int(args.down)
    x -= int(args.left)
    x += int(args.right)

dist_in_float=(math.sqrt (x*x + y*y))
round_off =int(round(dist_in_float))

print("Exact didstance:",dist_in_float)

print("Distance after Round Off:", round_off)
