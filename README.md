# python_task

This git Repo include a number of tasks done in python.

## How to Run Code

- First go to green **Clone** option on top right side and copy the URL **Clone with HTTP**
- Here open your Linux `Terminal` and select a `Directory` where you want to work and run following command `git clone < URL >`
- Upon successful clone of repo set working directory to **python_task** using `cd`

### RUN Task 1

- View output of task 1 using the following command `python3 task_1.py`

### RUN Task 2

- View output of task 2 using the following command `python3 task_2.py --up #number --down number --right #number ---left #nummber`

### RUN Task 3

- View output of task 3 using the following command `python3 task_3.py`

### RUN Task 4

- View output of task 4 using the following command `python3 task_4.py -h` for help
- To pass iterator by command line run `python3 task_4.py -i #number`
- To load iterator from JSON file run `python3 task_4.py -j`

### RUN Task 5

- View output of task 5 using the following command `python3 task_5.py`

### RUN Task 6

- First install Virtualenv using `pip install virtualenv`
- Creat virualenv for you task using `virualenv venv_name` and activate it by using `source venv_name/bin/activate`
- Install Asyncio in that env usingn `pip install asyncio`
- View output of task 6 using the following command `python3 find_divisibles.py` and `python3 find_divisibles_async.py`
- Once you are done, deactivate the virtual environment by the following command `deactivate`

### RUN Task 7

- First Run the server.py file using `python3 server.py`
- After Running the server.py file run the client.py file along with the client_messages.json file locaton as command line argument `python3 client.py --location /path/to/client_message.json/file`

### RUN Task 8

- View output of task 8 using following command `python3 task_8.py --location /path/to/latency.txt/file` file along with the msg_latency.json file locaton as command line argument

### RUN Task 9

- View output of task 9 using following command `python3 task_9`
- First enter the number of words you want to enter and Enter number one by one
